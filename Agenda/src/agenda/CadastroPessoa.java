import agenda.ControlePessoa;
import java.util.Scanner;
import java.io.*;

public class CadastroPessoa{

  public static void main(String[] args) throws IOException{
    //burocracia para leitura de teclado
    InputStream entradaSistema = System.in;
    InputStreamReader leitor = new InputStreamReader(entradaSistema);
    BufferedReader leitorEntrada = new BufferedReader(leitor);
    String entradaTeclado;


    //instanciando objetos do sistema
    ControlePessoa umControle = new ControlePessoa();
    Pessoa umaPessoa = new Pessoa();
    
    //Declarando variável de opção do usuário
    boolean continuar = true;
    int opcao;
    Scanner entrada = new Scanner(System.in);

while(continuar){


    //Menu de opções
    System.out.println("1 - Adicionar uma Pessoa ");
    System.out.println("2 - Pesquisar uma Pessoa");
    System.out.println("3 - Remover uma Pessoa ");
    System.out.println("4 - Concluir cadastro ");
    System.out.println("Entre com a opção desejada [1,2,3,4]: ");
    opcao = entrada.nextInt();

	switch( opcao )
	{
		case 1:

		//interagindo com usuário
		    System.out.println("Digite o nome da Pessoa:");
		    entradaTeclado = leitorEntrada.readLine();
		    String umNome = entradaTeclado;
		    umaPessoa.setNome(umNome);

		    System.out.println("Digite o sexo da Pessoa:");
		    entradaTeclado = leitorEntrada.readLine();
		    String umSexo = entradaTeclado;
		    umaPessoa.setSexo(entradaTeclado);

		    System.out.println("Digite a idade da Pessoa:");
		    entradaTeclado = leitorEntrada.readLine();
		    String umIdade = entradaTeclado;
		    umaPessoa.setIdade(entradaTeclado);

		    System.out.println("Digite o telefone da Pessoa:");
		    entradaTeclado = leitorEntrada.readLine();
		    String umTelefone = entradaTeclado;
		    umaPessoa.setTelefone(umTelefone);

		    System.out.println("Digite o email da Pessoa:");
		    entradaTeclado = leitorEntrada.readLine();
		    String umEmail = entradaTeclado;
		    umaPessoa.setEmail(entradaTeclado);

		    System.out.println("Digite o hangout da Pessoa:");
		    entradaTeclado = leitorEntrada.readLine();
		    String umHangout = entradaTeclado;
		    umaPessoa.setHangout(entradaTeclado);

		    System.out.println("Digite o endereço da Pessoa:");
		    entradaTeclado = leitorEntrada.readLine();
		    String umEndereço = entradaTeclado;
		    umaPessoa.setEndereco(entradaTeclado);

		    System.out.println("Digite o RG da Pessoa:");
		    entradaTeclado = leitorEntrada.readLine();
		    String umRg = entradaTeclado;
		    umaPessoa.setRg(entradaTeclado);

		    System.out.println("Digite o CPF da Pessoa:");
    		    entradaTeclado = leitorEntrada.readLine();
    		    String umCpf = entradaTeclado;
    		    umaPessoa.setCpf(entradaTeclado);

    		//adicionando uma pessoa na lista de pessoas do sistema
    		    String mensagem = umControle.adicionar(umaPessoa);

		//conferindo saída
    		    System.out.println("=================================");
    		    System.out.println(mensagem);
    		    System.out.println("=)");
		    break;

		case 2:

		    //Pesquisando uma pessoa na lista de pessoas do sistema
		    System.out.println("Digite o nome da Pessoa que deseja pesquisar:");
		    entradaTeclado = leitorEntrada.readLine();
		    String nomePesquisar = entradaTeclado;
		    Pessoa mensagem3 = umControle.pesquisar(nomePesquisar);

		    //conferindo saída
		    System.out.println("=================================");
		    System.out.println("Pessoa encontrada!\n");
		    System.out.println(mensagem3.getNome() + " tem o telefone " + mensagem3.getTelefone() + ", seu email é " + mensagem3.getEmail() + ", seu hangout " + mensagem3.getHangout() + ", mora no endereço " + mensagem3.getEndereco() + ", seu RG é " + mensagem3.getRg() + " e seu CPF " + mensagem3.getCpf() + ".\n");



		    break;


		case 3:

		    //removendo uma pessoa da lista de pessoas do sistema
		    System.out.println("Digite o nome da Pessoa que deseja remover:");
		    entradaTeclado = leitorEntrada.readLine();
		    String nomeRemover = entradaTeclado;
		    Pessoa pessoaRemover = umControle.pesquisar(nomeRemover);
		    String mensagem2 = umControle.remover(pessoaRemover);

		    //conferindo saída
			System.out.println("=================================");
			System.out.println(mensagem2);
		        System.out.println("=)");
		break;		    



		default:
		    continuar = false;
		    System.out.println("Cadastro concluído!!!");

	}

     }


  }

}

