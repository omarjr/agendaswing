/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package agenda.visao;

import agenda.controle.ControlePessoa;
import agenda.modelo.Pessoa;
import javax.swing.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/** 
 *
 * @author omarfsjunior
 */
public class telaAgenda extends JFrame {
    private final byte SEXO_MASCULINO_INDICE = 0;
    private final byte SEXO_FEMININO_INDICE = 1;
    private final char SEXO_MASCULINO_VALOR = 'M';
    private final char SEXO_FEMININO_VALOR = 'F';
    private Pessoa umaPessoa;
    private ControlePessoa controlePessoa;
    private boolean modoAlteracao;
    private boolean novoRegistro;

    /**
     * Creates new form telaAgenda
     */
    public telaAgenda() {
        initComponents();
        this.habilitarDesabilitarCampos();
        this.controlePessoa = new ControlePessoa();
        this.jTableListaContatos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    private void limparCampos(){
        jTextFieldCpf.setText(null);
        jTextFieldEmail.setText(null);
        jTextFieldEndereco.setText(null);
        jTextFieldHangout.setText(null);
        jTextFieldIdade.setText(null);
        jTextFieldNome.setText(null);
        jTextFieldRg.setText(null);
        jTextFieldTelefone.setText(null);
        jComboBoxSexo.setSelectedIndex(0);
    }
    
    private void preencherCampos(){
        
        jTextFieldCpf.setText(umaPessoa.getCpf());
        jTextFieldEmail.setText(umaPessoa.getEmail());
        jTextFieldEndereco.setText(umaPessoa.getEndereco());
        jTextFieldHangout.setText(umaPessoa.getHangout());
        jTextFieldIdade.setText(umaPessoa.getIdade());
        jTextFieldNome.setText(umaPessoa.getNome());
        jTextFieldRg.setText(umaPessoa.getRg());
        jTextFieldTelefone.setText(umaPessoa.getTelefone());
        
        switch (umaPessoa.getSexo()){
            case SEXO_MASCULINO_VALOR:
                jComboBoxSexo.setSelectedIndex(SEXO_MASCULINO_INDICE);
                break;
            case SEXO_FEMININO_VALOR:
                jComboBoxSexo.setSelectedIndex(SEXO_FEMININO_INDICE);
                break;
        }
        
    }
    
    private boolean validarCampos(){
        if (jLabelNome.getText().trim().length() == 0){
            this.exibirInformacao("O valor do campo 'Nome' não foi informado.");
            jLabelNome.requestFocus();
            return false;
        }
        return true;
    }
    


    private void habilitarDesabilitarCampos(){
        boolean registroSelecionado = (umaPessoa != null);
        
        jTextFieldCpf.setEnabled(modoAlteracao);
        jTextFieldEmail.setEnabled(modoAlteracao);
        jTextFieldHangout.setEnabled(modoAlteracao);       
        jTextFieldIdade.setEnabled(modoAlteracao);                
        jTextFieldNome.setEnabled(modoAlteracao);                
        jTextFieldRg.setEnabled(modoAlteracao);                
        jTextFieldEndereco.setEnabled(modoAlteracao);                
        jTextFieldTelefone.setEnabled(modoAlteracao);                
        jComboBoxSexo.setEnabled(modoAlteracao);                
        jButtonCancelar.setEnabled(modoAlteracao);                
        jButtonEditar.setEnabled(modoAlteracao == false && registroSelecionado == true);                
        jButtonExcluir.setEnabled(modoAlteracao == false && registroSelecionado == true);                
        jButtonNovo.setEnabled(modoAlteracao == false);                
        jButtonPesquisar.setEnabled(modoAlteracao == false);                
        jButtonSalvar.setEnabled(modoAlteracao);                
        jComboBoxSexo.setEnabled(modoAlteracao);
        jTableListaContatos.setEnabled(modoAlteracao == false);
        
        
    }
    
    private void salvarRegistro(){
        
        Pessoa umaPessoa = new Pessoa();
        
        if (this.validarCampos() == false) {
            return;
        }
        
         if (novoRegistro == true){
             umaPessoa = new Pessoa(jTextFieldNome.getText());
         } else {
             umaPessoa.setNome(jTextFieldNome.getText()); 
         }
         umaPessoa.setCpf(jTextFieldCpf.getText());         
         umaPessoa.setEmail(jTextFieldEmail.getText());
         umaPessoa.setEndereco(jTextFieldEndereco.getText());
         umaPessoa.setHangout(jTextFieldHangout.getText());
         umaPessoa.setIdade(jTextFieldIdade.getText());
         umaPessoa.setNome(jTextFieldNome.getText());
         umaPessoa.setRg(jTextFieldRg.getText());
         umaPessoa.setTelefone(jTextFieldTelefone.getText());
         
         if (novoRegistro == true){
             controlePessoa.adicionar(umaPessoa);
         }
         
         modoAlteracao = false;
         novoRegistro = false;
         
         this.carregarListaPessoas();
         this.habilitarDesabilitarCampos();
    }
    

    
    private void carregarListaPessoas(){
        ArrayList<Pessoa> listaPessoas;
        listaPessoas = controlePessoa.getListaPessoas();
        DefaultTableModel model = (DefaultTableModel) jTableListaContatos.getModel();
        model.setRowCount(0);
        for (Pessoa umaPessoa : listaPessoas){
            model.addRow(new String[]{umaPessoa.getNome(), umaPessoa.getTelefone(), umaPessoa.getEmail()});
        }
        jTableListaContatos.setModel(model);
    }
    
    private void exibirInformacao(String info){
        JOptionPane.showMessageDialog(this, info, "Atenção", JOptionPane.INFORMATION_MESSAGE);
    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonNovo = new javax.swing.JButton();
        jButtonEditar = new javax.swing.JButton();
        jButtonExcluir = new javax.swing.JButton();
        jButtonPesquisar = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();
        jLabelListaContatos = new javax.swing.JLabel();
        jScrollPane = new javax.swing.JScrollPane();
        jTableListaContatos = new javax.swing.JTable();
        jTabbedPaneInformacoesGerais = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabelNome = new javax.swing.JLabel();
        jLabelTelefone = new javax.swing.JLabel();
        jLabelIdade = new javax.swing.JLabel();
        jLabelSexo = new javax.swing.JLabel();
        jLabelEmail = new javax.swing.JLabel();
        jLabelHangout = new javax.swing.JLabel();
        jLabelEndereco = new javax.swing.JLabel();
        jLabelRg = new javax.swing.JLabel();
        jLabelCpf = new javax.swing.JLabel();
        jTextFieldNome = new javax.swing.JTextField();
        jTextFieldTelefone = new javax.swing.JTextField();
        jTextFieldIdade = new javax.swing.JTextField();
        jComboBoxSexo = new javax.swing.JComboBox();
        jTextFieldEmail = new javax.swing.JTextField();
        jTextFieldHangout = new javax.swing.JTextField();
        jTextFieldEndereco = new javax.swing.JTextField();
        jTextFieldRg = new javax.swing.JTextField();
        jTextFieldCpf = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButtonNovo.setText("Novo");
        jButtonNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNovoActionPerformed(evt);
            }
        });

        jButtonEditar.setText("Editar");
        jButtonEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditarActionPerformed(evt);
            }
        });

        jButtonExcluir.setText("Excluir");
        jButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExcluirActionPerformed(evt);
            }
        });

        jButtonPesquisar.setText("Pesquisar...");
        jButtonPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPesquisarActionPerformed(evt);
            }
        });

        jButtonSalvar.setText("Salvar");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        jLabelListaContatos.setText("Lista de Contatos:");

        jTableListaContatos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "", null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Nome", "Telefone", "Email"
            }
        ));
        jTableListaContatos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableListaContatosMouseClicked(evt);
            }
        });
        jScrollPane.setViewportView(jTableListaContatos);

        jLabelNome.setText("Nome:");

        jLabelTelefone.setText("Telefone:");

        jLabelIdade.setText("Idade:");

        jLabelSexo.setText("Sexo:");

        jLabelEmail.setText("E-mail:");

        jLabelHangout.setText("Hangout:");

        jLabelEndereco.setText("Endereço:");

        jLabelRg.setText("RG:");

        jLabelCpf.setText("CPF:");

        jTextFieldNome.setText("<digite aqui o nome do contato>");

        jTextFieldTelefone.setText("<digite aqui o telefone do contato>");

        jTextFieldIdade.setText("<digite aqui a idade do contato>");

        jComboBoxSexo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Masculino", "Feminino" }));

        jTextFieldEmail.setText("<digite aqui o email do contato>");

        jTextFieldHangout.setText("<digite aqui o hangout do contato>");

        jTextFieldEndereco.setText("<digite aqui o endereço do contato>");

        jTextFieldRg.setText("<digite aqui o RG do contato>");

        jTextFieldCpf.setText("<digite aqui o CPF do contato>");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabelCpf)
                    .addComponent(jLabelRg)
                    .addComponent(jLabelEndereco)
                    .addComponent(jLabelHangout)
                    .addComponent(jLabelTelefone)
                    .addComponent(jLabelNome)
                    .addComponent(jLabelIdade)
                    .addComponent(jLabelSexo)
                    .addComponent(jLabelEmail))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jComboBoxSexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jTextFieldEmail, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jTextFieldIdade, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE))
                    .addComponent(jTextFieldHangout, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, 248, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldRg, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(566, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNome)
                    .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelTelefone)
                    .addComponent(jTextFieldTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelIdade)
                    .addComponent(jTextFieldIdade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelSexo)
                    .addComponent(jComboBoxSexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelEmail)
                    .addComponent(jTextFieldEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelHangout)
                    .addComponent(jTextFieldHangout, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelEndereco)
                    .addComponent(jTextFieldEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelRg)
                    .addComponent(jTextFieldRg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelCpf)
                    .addComponent(jTextFieldCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPaneInformacoesGerais.addTab("Informações Gerais", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPaneInformacoesGerais, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButtonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabelListaContatos))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(16, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonNovo)
                    .addComponent(jButtonEditar)
                    .addComponent(jButtonExcluir)
                    .addComponent(jButtonPesquisar))
                .addGap(18, 18, 18)
                .addComponent(jLabelListaContatos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jTabbedPaneInformacoesGerais, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonSalvar)
                    .addComponent(jButtonCancelar))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovoActionPerformed
        // TODO add your handling code here:
        umaPessoa = null;
        modoAlteracao = true;
        novoRegistro = true;
        this.limparCampos();
        this.habilitarDesabilitarCampos();
        this.jTextFieldNome.requestFocus();
    }//GEN-LAST:event_jButtonNovoActionPerformed

    private void jButtonExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirActionPerformed
        // TODO add your handling code here:
        this.controlePessoa.remover(umaPessoa);
        umaPessoa = null;
        this.limparCampos();
        this.carregarListaPessoas();
        this.habilitarDesabilitarCampos();
    }//GEN-LAST:event_jButtonExcluirActionPerformed

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        // TODO add your handling code here:
        this.salvarRegistro();
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        // TODO add your handling code here:
        if (novoRegistro == true) {
            this.limparCampos();
        } else {
            this.preencherCampos();
        }
        modoAlteracao = false;
        novoRegistro = false;
        this.habilitarDesabilitarCampos();
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jButtonPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPesquisarActionPerformed
        // TODO add your handling code here:
    String pesquisa = JOptionPane.showInputDialog("Informe o nome do Boxeador.");
    if (pesquisa != null) {
        this.pesquisarPessoa(pesquisa);
    }
    }//GEN-LAST:event_jButtonPesquisarActionPerformed

    private void jButtonEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditarActionPerformed
        // TODO add your handling code here:
        modoAlteracao = true;
        novoRegistro = false;
        this.habilitarDesabilitarCampos();
        this.jTextFieldNome.requestFocus();
    }//GEN-LAST:event_jButtonEditarActionPerformed

    private void jTableListaContatosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListaContatosMouseClicked
        // TODO add your handling code here:
        if (jTableListaContatos.isEnabled()) {
        DefaultTableModel model = (DefaultTableModel) jTableListaContatos.getModel();
        String nome = (String) model.getValueAt(jTableListaContatos.getSelectedRow(), 0);
        this.pesquisarPessoa(nome);
        }
    }//GEN-LAST:event_jTableListaContatosMouseClicked

    private void pesquisarPessoa(String nome){
        Pessoa pessoaPesquisada = controlePessoa.pesquisar(nome);
        if (pessoaPesquisada == null){
            exibirInformacao("Pessoa não encontrada.");
        } else{
            this.umaPessoa = pessoaPesquisada;
            this.preencherCampos();
            this.habilitarDesabilitarCampos();
        }
    }
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(telaAgenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(telaAgenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(telaAgenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(telaAgenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new telaAgenda().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonEditar;
    private javax.swing.JButton jButtonExcluir;
    private javax.swing.JButton jButtonNovo;
    private javax.swing.JButton jButtonPesquisar;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JComboBox jComboBoxSexo;
    private javax.swing.JLabel jLabelCpf;
    private javax.swing.JLabel jLabelEmail;
    private javax.swing.JLabel jLabelEndereco;
    private javax.swing.JLabel jLabelHangout;
    private javax.swing.JLabel jLabelIdade;
    private javax.swing.JLabel jLabelListaContatos;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JLabel jLabelRg;
    private javax.swing.JLabel jLabelSexo;
    private javax.swing.JLabel jLabelTelefone;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane;
    private javax.swing.JTabbedPane jTabbedPaneInformacoesGerais;
    private javax.swing.JTable jTableListaContatos;
    private javax.swing.JTextField jTextFieldCpf;
    private javax.swing.JTextField jTextFieldEmail;
    private javax.swing.JTextField jTextFieldEndereco;
    private javax.swing.JTextField jTextFieldHangout;
    private javax.swing.JTextField jTextFieldIdade;
    private javax.swing.JTextField jTextFieldNome;
    private javax.swing.JTextField jTextFieldRg;
    private javax.swing.JTextField jTextFieldTelefone;
    // End of variables declaration//GEN-END:variables
}
